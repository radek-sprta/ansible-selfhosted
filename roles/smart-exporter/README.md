Prometheus Smart Exporter
========================
A role to run Smart exporter script.

Role Variables
--------------
- `smart_exporter_directory`: Directory for saving measurements. Defaults to `/var/lib/node_exporter/textfile_collector`.

Example Playbook
----------------
```yaml
- hosts: all

  roles:
     - smart_exporter
```

License
-------
MIT
