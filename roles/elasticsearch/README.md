Elasticsearch
=============
A role to run Elasticsearch container.

Role Variables
--------------
- `elasticsearch_container_name`: Container name, defaults to elasticsearch.
- `elasticsearch_network_name`: Name of elasticsearch network, defaults to elasticsearch.
- `elasticsearch_version`: Elasticsearch version, defaults to 7.9.3.
- `elasticsearch_volume`: Elasticsearch volume, defaults to /opt/elasticsearch.

Example Playbook
----------------
```yaml
- hosts: elasticsearch

  roles:
     - elasticsearch
```

License
-------
MIT
