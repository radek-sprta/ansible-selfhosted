UrBackup-client
===============
A role to run urbackup-client container.

Role Variables
--------------
- `urbackup_client_manual_backup_directories`: List of directories to backup.
- `urbackup_client_manual_installer_path`: Path to download installer to. Defaults to /tmp/urbackup_client_installer.sh.
- `urbackup_client_manual_version`: Client version to download to. Defaults to 2.4.10.

Example Playbook
----------------
```yaml
- hosts: clients
  roles:
     - { role: urbackup-client-manual, urbackup_client_manual_backup_directories: ["/backup"] }
```

License
-------
MIT
