Transmission
============
A role to run Transmission container.

Role Variables
--------------
    transmission_container_name: transmission

Container name.

    transmission_config_volume: /opt/transmission
    transmission_downloads_volume: /downloads
    transmission_watch_volume: /watch

Volumes for configuration, downloads and watched directory respectively.

    transmission_user: transmission
    transmission_password: mysecret

Login credentials.

Example Playbook
----------------
```yaml
- hosts: transmission

  roles:
     - transmission
```

License
-------
MIT
