Syncthing
============
A role to run Syncthing container.

Role Variables
--------------
    syncthing_container_name: syncthing

Container name.

    syncthing_volume: /opt/syncthing

Volume for Syncthing data.

    syncthing_port: 8384

Port for running the WebGUI.

Example Playbook
----------------
```yaml
- hosts: syncthing

  roles:
     - syncthing
```

License
-------
MIT
