Deploy
======
Creates deploy user, who is part of docker group and adds his SSH key.


Role Variables
--------------
- `deploy_docker_group`: Docker group name.
- `deploy_user`: Deploy user name.
- `deploy_user_home`: Deploy user home.
- `deploy_user_key`: Deploy user public SSH key.

Example Playbook
----------------
```
- hosts: servers
  roles:
     - { role: deploy, deploy_user_key: "ssh-rsa ..." }
```

License
-------
MIT
