Unbound-authoritative
=====================
A role to run Unbound authoritative DNS container.

Role Variables
--------------
- `unbound_volume`: Unbound volume. Defaults to /opt/unbound.
- `unbound_container_name`: Container name, defaults to unbound.
- `unbound_records`: Dictionary of records.

Example Playbook
----------------
```yaml
- hosts: dns

  vars:
    unbound_records:
      example.org.:
        type: A
        value: 1.2.3.4
      1.2.3.4:
        type: PTR
        value: example.org.
            
  roles:
     - unbound
```

License
-------
MIT
