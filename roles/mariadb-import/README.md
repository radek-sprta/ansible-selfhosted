Mariadb-import
==============
A role to import a MariaDB database and create login credentials.

Role Variables
--------------
- `mariadb_import_admin_password`: Password for database admin user.
- `mariadb_import_admin_user`: Login for database admin user.
- `mariadb_import_container_name`: Database container name, defaults to mariadb.
- `mariadb_import_dump_path`: Path to database dump, empty by default.
- `mariadb_import_lock`: Database import lock file, defaults to /opt/{{ mariadb_import_container_name }}/{{ mariadb_import_name}}_imported.lock.
- `mariadb_import_name`: Database name, defaults to database.
- `mariadb_import_network`: Network the Database container runs on. Defaults to database.
- `mariadb_import_password`: Database password to create.
- `mariadb_import_user`: Database user to create.

Example Playbook
----------------
```yaml
- hosts: servers
  roles:
     - { role: username.rolename, mariadb_import_password: sfdsfja234, mariadb_import_dump_path: ~/dumps/database.sql, selfoss_database_admin_password: skfjf2343 }
```

License
-------
MIT
