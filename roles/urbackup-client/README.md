UrBackup-client
===============
A role to run urbackup-client container.

Role Variables
--------------
- `urbackup_client_authkey`: Client authentization key.
- `urbackup_client_backup_directories`: Volume for backups, defaults to /opt/urbackup/backups.
- `urbackup_client_backup_volume`: List of directories to backup.
- `urbackup_client_container_name`: Container name, defaults to urbackup-client.
- `urbackup_client_name`: Client name. Defaults to hostname.
- `urbackup_client_server_name`: Hostname or IP address of the server.

Example Playbook
----------------
```yaml
- hosts: clients
  roles:
     - { role: urbackup-client, urbackup_client_server_name: backup.example.org, urbackup_client_backup_directories: ["/backup"] }
```

License
-------
MIT
