Grafana
=======
A role to run Grafana container.

Role Variables
--------------
- `grafana_admin_password`: Password to Grafana.
- `grafana_admin_user`: Grafana user, defaults to admin.
- `grafana_container_name`: Container name, defaults to grafana.
- `grafana_domain`: Domain to use, defaults to grafana.example.org.
- `grafana_influx_database`: Influx database name, defaults to grafana.
- `grafana_influx_host`: Influx database host, defaults to influx.
- `grafana_influx_network`: Network for communicating with Influx, defaults to influx.
- `grafana_influx_password`: Influx password.
- `grafana_influx_port`: Influx database port, defaults to 8086.
- `grafana_influx_user`: Influx user, defaults to grafana.
- `grafana_install_plugins`: Extra plugins to install.
- `grafana_proxy_network`: Network for communication with proxy, defaults to proxy.
- `grafana_volume`: Volume directory, defaults to /opt/grafana.

Example Playbook
----------------
```yaml
- hosts: grafana

  vars:
    grafana_influx_password: secret_password
    grafana_admin_password: secret_password2
            
  roles:
     - grafana
```

License
-------
MIT
