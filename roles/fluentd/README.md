Fluentd
======
A role to run Fluentd container.

Role Variables
--------------
- `fluentd_container_name`: Container name, defaults to fluentd.
- `fluentd_elasticsearch_host`: Elasticsearch host. Defaults to http://elasticsearch:9200.
- `fluentd_hostname`: Fluentd hostname.
- `fluentd_network_name`: Name of fluentd network, defaults to fluentd.
- `fluentd_proxy_network`: Name of the proxy network, defaults to proxy.
- `fluentd_version`: Fluentd version, defaults to 7.9.3.

Example Playbook
----------------
```yaml
- hosts: fluentd

  roles:
     - fluentd
```

License
-------
MIT
