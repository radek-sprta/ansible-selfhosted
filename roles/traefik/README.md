Traefik
=======
Deploy a Traefik reverse proxy for Docker containers.

Role Variables
--------------
- `traefik_container_name`: Name of the container. Defautls to `proxy`.
- `traefik_data_dir`: Directory for Traefik data, defaults to `/opt/traefik`.
- `traefik_dashboard_domain`: Domain to use, defaults `traefik.example.com`.
- `traefik_dashboard_user`: Traefik dashboard username, defaults to `admin`.
- `traefik_dashboard_password`: Traefik dashboard password.
- `traefik_email`: Email for Let's Encrypt account, defaults to `email@example.org`.
- `traefik_exporter_host`: Metrics host, defaults to `""`.
- `traefik_exporter_network`: Network to communicate with Prometheus. Defaults to `prometheus`.
- `traefik_exporter_port`: Metrics port, defaults to `8082`.
- `traefik_network_name`: Network to communicate with the proxied containers. Defaults to `proxy`.

Example Playbook
----------------
```
- hosts: servers

  roles:
     - traefik
```

License
-------
MIT
