Lychee
======
A role to run Lychee on Docker.

Role Variables
--------------
- `lychee_container_name`: Container name, defaults to lychee.
- `lychee_data_volume`: Data volume, defaults to /opt/lychee/data.
- `lychee_uploads`: Lychee uploads directory with uploaded pictures.
- `lychee_startup_delay`: How long to wait for the database. Defaults to 0.
- `lychee_database_container_name`: Database container name, defaults to lychee_db.
- `lychee_database_dump_path`: Path to database dump.
- `lychee_database_name`: Database name, defaults to lychee.
- `lychee_database_password`: Database password.
- `lychee_database_port`: Port to connect to.
- `lychee_database_root_password`: Password for database admin user.
- `lychee_database_root_user`: Login for database admin user.
- `lychee_database_user`: Database user, default lychee.

Example Playbook
----------------
```yaml
- hosts: servers
  roles:
     - { role: username.rolename, lychee_database_password: sfdsfja234, lychee_database_dump_path: ~/dumps/lychee.sql, lychee_database_root_password: skfjf2343 }
```

License
-------
MIT
