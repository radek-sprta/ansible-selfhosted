Proxy
=====
Deploy a Caddy reverse proxy for Docker containers.

Role Variables
--------------
- `proxy_caddyfile_dir`: Directory for Caddyfile data. Defaults to `/opt/proxy`.
- `proxy_container_name`: Name for the container. Defaults to `proxy`.
- `proxy_email`: Used for Let's Encrypt alerts.
- `proxy_network_name`: Network to communicate with the proxied containers. Defaults to `proxy`.
- `proxy_domains`: Dictionary of domains to proxy, with container name and ports its listening on. Defines like this:

```
proxy_domains:
  example.org:
    container: example
    port: 8080
```

Example Playbook
----------------
```
- hosts: servers

  vars:
    proxy_domains:
      example.org:
        container: example
        port: 8080

  roles:
     - proxy
```

License
-------
MIT
