MariaDB
=======
Role to run MariaDB in a Docker.

Role Variables
--------------
### General
`mariadb_container_name`: MariaDB container name. Defaults to mariadb.
`mariadb_external_containers`: List of containers, that should be in the same network.
`mariadb_network_name`: Network for inter-container communication. Defaults to mariadb.
`mariadb_root_password`: Root password for the database.
`mariadb_version`: Version to run. Defaults to 10.4.
`mariadb_volume`: Data storage. Defaults to /var/lib/mysql.

### Exporter options
`mariadb_exporter_install`: Whether to install MariaDB Prometheus exporter. Defaults to true.
`mariadb_exporter_user`: MariaDB Prometheus exporter user. Defaults to mariadb_exporter.

Example Playbook
----------------
```
- hosts: servers
  roles:
     - { role: mariadb, mariadb_root_password: ser@WR#vgs }
```

License
-------
MIT

Author Information
------------------
Radek Sprta <mail@radeksprta.eu>
