MiniDLNA
========
A role to run MiniDLNA container.

Role Variables
--------------
    minidlna_container_name: minidlna

Container name.

    minidlna_friendly_name: minidlna

Name for the MiniDLNA instance.

    minidlna_inotify: "yes"

Use inotify to check for new files.

    minidlna_volume: /opt/minidlna

Location of MiniDLNA media files.

Example Playbook
----------------
```yaml
- hosts: minidlna

  roles:
     - minidlna
```

License
-------
MIT
