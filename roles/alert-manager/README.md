Alert-manager
=============
A role to run Alert Manager container.

Role Variables
--------------
- `alertmanager_container_name`: Container name, defaults to prometheus.
- `alertmanager_prometheus_network`: Network to communicate with Prometheus. Defaults to prometheus.
- `alertmanager_pagerduty_key`: API key for Pager Duty.
- `alertmanager_volume`: Volume directory, defaults to /opt/prometheus.

Example Playbook
----------------
```yaml
- hosts: alertmanager

  vars:
    alertmanager_pagerduty_key: secret_key
            
  roles:
     - alertmanager
```

License
-------
MIT
