Monica
======
A role to run Monica CRM on Docker.

Role Variables
--------------
- `monica_app_env`: Run environment, default production.
- `monica_app_key`: 32 character app key, required.
- `monica_app_trusted_proxies`: List of proxies Monica will trust, default to any.
- `monica_app_url`: URL to run on.
- `monica_container_name`: Container name, defaults to monica.
- `monica_data_volume`: Volume for data, defaults to /opt/monica/storage.
- `monica_database_admin_password`: Password for database admin user.
- `monica_database_admin_user`: Login for database admin user.
- `monica_database_container_name`: Database container name, defaults to monica_db.
- `monica_database_dump_path`: Path to database dump.
- `monica_database_lock`: Database import lock file, defaults to /opt/monica/database_imported.lock.
- `monica_database_name`: Database name, defaults to monica.
- `monica_database_password`: Database password.
- `monica_database_user`: Database user, default monica.
- `monica_database_volume`: Database data directory, default /var/lib/mysql.

Example Playbook
----------------
```yaml
- hosts: servers
  roles:
     - { role: username.rolename, monica_database_password: sfdsfja234, monica_database_dump_path: ~/dumps/monica.sql }
```

License
-------
MIT
