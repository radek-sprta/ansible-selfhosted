MariaDB-backup
==============
A role to run mariadb-backup container.

Role Variables
--------------
- `mariadb_backup_admin_password`: Password for database admin user.
- `mariadb_backup_admin_user`: Login for database admin user.
- `mariadb_backup_container_name`: Container name, defaults to mariadb-backup.
- `mariadb_backup_database_is_container`: Whether MariaDB runs as container. Defaults to true.
- `mariadb_backup_directory`: Directory to save backups to. Defaults to /var/lib/backup.
- `mariadb_backup_host`: MariaDB host. Defaults to mariadb.
- `mariadb_backup_network`: Network MariaDB container runs in. Defaults to mariadb.
- `mariadb_backup_password`: Backup user password.
- `mariadb_backup_period`: Backup period in cron syntax. Defaults to "0 0 * * *".
- `mariadb_backup_port`: Port to connect to. Defaults to 3306.
- `mariadb_backup_user`: Backup user, defaults to mariadb_backup.

Example Playbook
----------------
```yaml
- hosts: servers
  roles:
     - { role: username.rolename, mariadb_backup_admin_password: sfdsfja234 }
```

License
-------
MIT
