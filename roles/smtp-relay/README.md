SMTP-relay
==========
A simple SMTP relay server.

Role Variables
--------------
- `smtp_relay_container_name`: Container name, defaults to postfix.
- `smtp_relay_domains`: Domains to relay.
- `smtp_relay_hostname`: SMTP host name.
- `smtp_relay_network`: Network for communication with other containers, defaults to postfix.
- `smtp_relay_volume`: Directory for persistent data, defaults to /opt/smtp_relay/.

Example Playbook
----------------
```yaml
- hosts: servers
  roles:
     - { role: smtp-relay, smtp_relay_hostname: smtp.example.org, smtp_relay_domains: example.org }
```

License
-------
MIT
