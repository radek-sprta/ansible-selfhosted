OpenEats
========
A role to run OpenEats on Docker.

Role Variables
--------------
- `openeats_container_name`: Container name, defaults to openeats.
- `openeats_uploads`: Upload folder. Empty by default.
- `openeats_url`: Domain URL.
- `openeats_volume_home`: Persistent storage home, defaults to /opt/openeats/.
- `openeats_database_container_name`: Database container name, defaults to openeats_db.
- `openeats_database_dump_path`: Path to database dump.
- `openeats_database_name`: Database name, defaults to openeats.
- `openeats_database_password`: Database password.
- `openeats_database_port`: Port to connect to.
- `openeats_database_root_password`: Password for database admin user.
- `openeats_database_root_user`: Login for database admin user.
- `openeats_database_user`: Database user, default openeats.

Example Playbook
----------------
```yaml
- hosts: servers
  roles:
     - { role: username.rolename, openeats_database_password: sfdsfja234, openeats_database_dump_path: ~/dumps/openeats.sql, openeats_database_root_password: skfjf2343 }
```

License
-------
MIT
