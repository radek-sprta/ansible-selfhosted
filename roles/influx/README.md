Influx
======
A role to run InfluxDB container.

Role Variables
--------------
- `influx_admin_password`: Admin password.
- `influx_admin_user`: Admin user, defaults to admin.
- `influx_container_name`: Container name, defautls to influx.
- `influx_database`: Database to create, defaults to influx.
- `influx_network_name`: Name of influx network, defaults to influx.
- `influx_ro_user_password`: Read only user password.
- `influx_ro_user`: Read only user to create, defaults to influx\_ro.
- `influx_user_password`: User password.
- `influx_user`: User to create, defaults to influx.
- `influx_volume`: InfluxDB volume, defaults to /opt/influx.

Example Playbook
----------------
```yaml
- hosts: influx

  vars:
    admin_password: super_secret_password
    influx_user: myuser
    influx_user_password: secret_password
            
  roles:
     - influx
```

License
-------
MIT
