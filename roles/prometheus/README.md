Prometheus
==========
A role to run Prometheus container.

Role Variables
--------------
- `prometheus_alertmanager_alerts`: List of Alert Manager alerts.
- `prometheus_alertmanager_host`: Alert Manager host.
- `prometheus_alertmanager_port`: Alert Manager port. Defaults to 9093.
- `prometheus_container_name`: Container name, defaults to prometheus.
- `prometheus_domain`: Prometheus domai to use, defaults to prometheus.example.org.
- `prometheus_extra_hosts`: List of extra DNS hosts.
- `prometheus_influx_database`: Influx database name, defaults to prometheus.
- `prometheus_influx_host`: Influx database host, defaults to influx.
- `prometheus_influx_network`: Network for communicating with Influx, defaults to influx.
- `prometheus_influx_password`: Influx password.
- `prometheus_influx_port`: Influx database port, defaults to 8086.
- `prometheus_influx_user`: Influx user, defaults to prometheus.
- `prometheus_proxy_network`: Network to communicate with the proxy, defaults to proxy.
- `prometheus_scrape_interval`: Scraping interval, defaults to 60s.
- `prometheus_scrape_jobs`: Scrape jobs configuration.
- `prometheus_volume`: Volume directory, defaults to /opt/prometheus.

Example Playbook
----------------
```yaml
- hosts: prometheus

  vars:
    prometheus_influx_password: secret_password
    prometheus_scrape_jobs:
      prometheus:
        - localhost:9090
            
  roles:
     - prometheus
```

License
-------
MIT
