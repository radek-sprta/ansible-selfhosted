Kibana
======
A role to run Kibana container.

Role Variables
--------------
- `kibana_container_name`: Container name, defaults to kibana.
- `kibana_elasticsearch_host`: Elasticsearch host. Defaults to http://elasticsearch:9200.
- `kibana_hostname`: Kibana hostname.
- `kibana_network_name`: Name of kibana network, defaults to kibana.
- `kibana_proxy_network`: Name of the proxy network, defaults to proxy.
- `kibana_version`: Kibana version, defaults to 7.9.3.

Example Playbook
----------------
```yaml
- hosts: kibana

  roles:
     - kibana
```

License
-------
MIT
