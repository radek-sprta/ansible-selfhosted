UrBackup-server
===============
A role to run urbackup-server container.

Role Variables
--------------
- `urbackup_server_backup_volume`: Volume for backups, defaults to /opt/urbackup/backups.
- `urbackup_server_container_name`: Container name, defaults to urbackup-server.
- `urbackup_server_create_user`: Create new user. Defaults to true.
- `urbackup_server_database_volume`: Volume for urbackup database, defaults to /opt/urbackup/database.
- `urbackup_server_gid`: UrBackup user id, defaults to 101.
- `urbackup_server_timezone`: Timezone, defaults to UTC.
- `urbackup_server_uid`: UrBackup group id, defaults to 101.

Example Playbook
----------------
```yaml
- hosts: servers
  roles:
     - urbackup-server
```

License
-------
MIT
