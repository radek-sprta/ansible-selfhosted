Selfoss
=======
A role to run Selfoss on Docker.

Role Variables
--------------
- `selfoss_container_name`: Container name, defaults to selfoss.
- `selfoss_data_volume`: Data volume, defaults to /opt/selfoss/data.
- `selfoss_db_type`: Database type to use, defaults to mysql.
- `selfoss_homepage`: Default selfoss page, defaults to unread.
- `selfoss_html_title`: Window title, defaults to Selfoss.
- `selfoss_items_lifetime`: Item lifetime, defaults 60.
- `selfoss_items_perpage`: Items per page, defaults to 50.
- `selfoss_log`: Log to standard output, defaults to true.
- `selfoss_password`: Selfoss password.
- `selfoss_rss_max_items`: Maximum items to keep, defaults to 300.
- `selfoss_rss_title`: RSS title, defaults to Selfoss Feed.
- `selfoss_user`: Login user, defaults to selfoss.
- `selfoss_database_admin_password`: Password for database admin user.
- `selfoss_database_admin_user`: Login for database admin user.
- `selfoss_database_container_name`: Database container name, defaults to selfoss_db.
- `selfoss_database_dump_path`: Path to database dump.
- `selfoss_database_lock`: Database import lock file, defaults to /opt/selfoss/database_imported.lock.
- `selfoss_database_name`: Database name, defaults to selfoss.
- `selfoss_database_password`: Database password.
- `selfoss_database_port`: Port to connect to.
- `selfoss_database_user`: Database user, default selfoss.

Example Playbook
----------------
```yaml
- hosts: servers
  roles:
     - { role: username.rolename, selfoss_password: sfdsfja234, selfoss_database_dump_path: ~/dumps/selfoss.sql, selfoss_database_root_password: skfjf2343 }
```

License
-------
MIT
